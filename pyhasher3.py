from hashpy import *
import pathlib
import sys
import argparse
from datetime import datetime
import csv
from xml import etree


__version__ = "3.0"

all_extensions = [".{}".format(alg) for alg in algorithms_available]
        

def make_logfile(args, hasher):
    fieldnames = ['File Path', 'File Size', 'Modified Time', 'Result']
    fieldnames.extend([a.upper() for a in hasher.algorithms])    
    logfile_path = pathlib.Path(args.logfile)
    extension = logfile_path.suffix.lower()
    if extension == '.csv':
        write_csv(logfile_path, fieldnames, hasher)
    elif extension == '.xml':
        write_xml(logfile_path, fieldnames, hasher)
    elif extension == '.txt':
        write_txt(logfile_path, hasher)
    elif extension in all_extensions:
        write_sum(logfile_path, hasher)
    else:
        pass
        
def write_txt(logfile_path, hasher):
    with logfile_path.open('w', encoding='utf-8') as fout:
        for hr in hasher.hash_records:
            lines = []
            lines.append(hr.path.as_posix())
            if hr.result == Hasher.HASHED:
                for alg in hasher.algorithms:
                    lines.append("{}: {}".format(alg.upper(), hr.hashes[alg].upper()))
            else:
                if hr.result == Hasher.NOT_HASHED:
                    lines.append('ERROR: Not Hashed ({})'.format(hr.error_msg))
                elif hr.result == Hasher.FILE_NOT_FOUND:
                    lines.append('ERROR: File Not Found ({})'.format(hr.error_msg))
            lines.append("="*50)
            lines.append('')
            fout.write(os.linesep.join(lines))
            
def write_sum(logfile_path, hasher):
    logfiles = {}
    for alg in hasher.algorithms:
        fp = logfile_path.parent / (logfile_path.stem + ".{}".format(alg))
        logfiles[alg] = open(fp.as_posix(), 'w', encoding='utf-8')
    for hr in hasher.hash_records:
        if hr.result == Hasher.HASHED:
            for alg in hasher.algorithms:
                line = "{} *{}{}".format(hr.hashes[alg],
                                         hr.path.as_posix(),
                                         os.linesep)
                logfiles[alg].write(line)
        else:
            if hr.result == Hasher.NOT_HASHED:
                h = '#ERROR: Not Hashed ({})'.format(hr.error_msg)
            elif hr.result == Hasher.FILE_NOT_FOUND:
                h = '#ERROR: File Not Found ({})'.format(hr.error_msg)
            else:
                h = '#ERROR'
            for alg in hasher.algorithms:
                line = "{} *{}{}".format(h,hr.path.as_posix(), os.linesep)
                logfiles[alg].write(line)
    for alg in hasher.algorithms:
        logfiles[alg].close()
    
def write_xml(logfile_path, fieldnames, hasher):
    with logfile_path.open('w', encoding='utf-8', newline='\n') as fout:
        pass
        

def write_csv(logfile_path, fieldnames, hasher):
    with logfile_path.open('w', newline='', encoding='utf-8') as fout:
        logfile = csv.writer(fout)
        logfile.writerow(fieldnames)
        # for hash values rows when there was an error and it wasn't hashed
        error_hashes = [None]*len(hasher.algorithms)
        for hr in hasher.hash_records:
            row = []
            if hr.result == Hasher.FILE_NOT_FOUND:
                row.extend([hr.path, None, None, 'File Not Found'])
            else:
                try:
                    dt = datetime.fromtimestamp(hr.path.stat().st_mtime).isoformat()
                    size = hr.path.stat().st_size                    
                except OSError:
                    dt = None
                    size = None
                row.extend([hr.path, size, dt])
            if hr.result == Hasher.HASHED:
                row.append('Hashed')
                for alg in hasher.algorithms:
                    row.append(hr.hashes[alg])
            else:
                if hr.result == Hasher.NOT_HASHED:
                    row.append('Not Hashed ({})'.format(hr.error_msg))
                row.extend(error_hashes)
            logfile.writerow(row)

def to_stdout(*to_print):
    try:
        print(*to_print, sep='')
    except UnicodeEncodeError:
        b = ''.join(to_print).encode(sys.stdout.encoding, errors='replace')
        print(b.decode(sys.stdout.encoding))
        
def setup(args):
    try:
        directory = pathlib.Path(args.directory).resolve()
    except FileNotFoundError:
        sys.exit("'{}' does not exist".format(args.directory))
    if not args.algorithms:
        hasher = Hasher()
    else:
        hasher = Hasher(*args.algorithms)
    return hasher, directory

def main(hasher, directory, args):
    for hr in hasher.hash_directory(directory, args.recursive, args.patterns):
        if args.relative:
            hr.path = hr.path.relative_to(directory)
        to_stdout(hr.path.as_posix())
        if hr.result == Hasher.HASHED:
            for alg in hasher.algorithms:
                print("{}: {}".format(alg.upper(), hr.hashes[alg].upper()))
        else:
            if hr.result == Hasher.NOT_HASHED:
                print('ERROR: Not Hashed ({})'.format(hr.error_msg))
            elif hr.result == Hasher.FILE_NOT_FOUND:
                print('ERROR: File Not Found ({})'.format(hr.error_msg))
        print("="*50)
        
logfile_help = """File to write results out to. The extension determines how it is written.
Can write out to text file (.txt), comma-separated values (.csv), or XML (.xml).
"""

pattern_help = """Filename patterns to use for matching files to hash."""
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--directory', metavar='DIRECTORY', default='.')
    parser.add_argument('-a', '--algorithms', nargs='+', choices=algorithms_available, metavar='|'.join(algorithms_available))
    parser.add_argument('-r', '--recursive', action='store_true')
    parser.add_argument('-p', '--patterns', nargs='+', metavar='PATTERN', help=pattern_help)
    parser.add_argument('--logfile', help=logfile_help)
    parser.add_argument('--version', action='store_true', help='show version and exit')
    parser.add_argument('--relative', action='store_true')
    args = parser.parse_args()
    if args.version:
        sys.exit('pyhasher version {}'.format(__version__))
    start_dt = datetime.now()
    hasher, directory = setup(args)
    try:
        main(hasher, directory, args)
        print("Completed")
    except KeyboardInterrupt:
        print("Keyboard Interrupt, Not Completed")
    finally:
        end_dt = datetime.now()
        print("Total Files Found: {:,}".format(hasher.files))
        print("Total Files Hashed: {:,}".format(hasher.hashed))
        print("Total Errors: {:,}".format(hasher.errors))
        print("Total Bytes Read: {:,}".format(hasher.read_bytes))
        print("Total Seconds: {:,}".format((end_dt - start_dt).total_seconds()))
        if args.logfile:
            print("Writing Logfile...")
            logfile_start = datetime.now()            
            make_logfile(args, hasher)
            logfile_end = datetime.now()
            logfile_write = (logfile_end - logfile_start).total_seconds()
            print("{:,} seconds to write log file".format(logfile_write))