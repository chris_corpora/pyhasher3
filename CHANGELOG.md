## 2.2 ##

* Assigned file name patterns an option `-p` or `--pattern` instead of placing at the end optionally which causes problems when placing
after options that have optional arguments.

* Added option to only hash a single file and print to standard output

* Refactored code for parsing command line options to make it easier to test and read

## 2.0.1 ##

* Gives precedence to algorithm name in file name or inferred hash value read from file. Inferred hash algorithm from line in file will only match to guaranteed algorithms md5, sha1, sha224, sha256, and sha512 since there is overlap in lengths between algorithms.

* Fixed bug in which algorithm for verification was not being used when it was obtained from the verification filename.

## 2.0 ##

* Added all hash functions available in Python hashlib module

* Default output file extension will be .pyh instead of pymd5, pysha1, etc.

* Added --fail-fast option to stop on first hash mismatch during verification

* Removed -N/--no-dir-headings and changed default behavior for output to log the relative path of each file next to the file hash separated by two spaces

* Created option --headings that will provide previous default behavior of logging relative directory path once with filenames and hashes below.

* Changed -L/--log-output to -o/--output

* Changed option --log-all to -L/--log-all

## 1.0.2 ##
* Fixes errors in the documentation

## 1.0.1 ##
* Changes to help documentation

## 1.0 ##
* First Release