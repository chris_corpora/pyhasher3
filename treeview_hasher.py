import tkinter as tk
from tkinter import ttk
from tkinter import font
import pathlib
import hashlib
import os
from pyhasher import Hasher
import sys
import threading
from tkinter import filedialog

def get_hashes():
    m_hasher = Hasher('md5')
    s_hasher = Hasher('sha1')
    start_dir = pathlib.Path(filedialog.askdirectory()).resolve()
    for root_dir, dirs, files in os.walk(start_dir.as_posix()):
        root_path = pathlib.Path(root_dir).resolve()
        current_dir = tree.insert("", "end", root_dir, text=root_path.as_posix(), open=True)
        for fn in files:
            fpath = pathlib.Path(root_dir) / fn
            s_hash = s_hasher.hash_file(fpath)
            m_hash = m_hasher.hash_file(fpath)
            tree.insert(current_dir, "end", fpath.as_posix(), text=fpath.name, values=(m_hash, s_hash))
            tree.update()
            
def threaded_get_hashes():
    p = threading.Thread(target=get_hashes)
    p.start()

if __name__ == '__main__':
    root = tk.Tk()
    root.title('TreeView Hasher')
    root.geometry('1200x800+500+500')
    tree = ttk.Treeview(root)
    style = ttk.Style()
    style.configure(root, font=('Lucida Console', 8))    
    hash_button = ttk.Button(root, text="Hash Directory")
    hash_button.configure(command=threaded_get_hashes)
    hash_button.pack(expand=False, side=tk.BOTTOM)
    tree["columns"]=("md5","sha1")
    tree.column("md5", width=8*(len(hashlib.new('md5').hexdigest())), stretch=False)
    tree.column("sha1", width=8*(len(hashlib.new('sha1').hexdigest())), stretch=False)
    tree.heading("md5", text="MD5")
    tree.heading("sha1", text="SHA1")
    tree.insert("", 0, iid="directory_column")
    tree.pack(expand=True, fill=tk.BOTH)
    root.mainloop()