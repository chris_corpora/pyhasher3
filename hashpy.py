import hashlib
import pathlib
import sys
import re
import os
import argparse
from datetime import datetime
import csv

KB = 2**10
MB = KB*KB
GB = KB*MB
TB = KB*GB

algorithms_available = hashlib.algorithms_guaranteed

#_hex_sizes = {alg: len(hashlib.new(alg).hexdigest()) for alg in algorithms_available}
#_hex_patterns = {alg: re.compile('([0-9]|[a-f]){{{}}}'.format(_hex_sizes[alg]), flags=re.IGNORECASE) for alg in _hex_sizes}

# If the file size is <= MAX_READ_SZ, it will be read all at once,
# otherwise the file will be read in READ_SZ chunks
MAX_READ_SZ = 100 * MB
READ_SZ = 32 * KB

def _normalize(algorithm):
    """Normalize the given algorithm name to match list of algorithms"""
    normed = algorithm.lower().replace('-', '')
    if normed in algorithms_available:
        return normed
    else:
        raise ValueError('{} is not known and/or available'.format(algorithm))

class HashRecord:
    
    def __init__(self, path, result, hashes, error_msg=None):
        self.path = path
        self.result = result
        self.hashes = hashes
        self.error_msg = error_msg

class _Hashes:
    
    __slots__ = ['_hashes']
    
    def __init__(self, algorithms, data=b''):
        self._hashes = {alg: hashlib.new(alg, data) for alg in algorithms}
        
    def update(self, data):
        for alg in self._hashes:
            self._hashes[alg].update(data)
            
    @property    
    def hexdigests(self):
        return {alg: self._hashes[alg].hexdigest() for alg in self._hashes}
    
    @property    
    def digests(self):
        return {alg: self._hashes[alg].digest() for alg in self._hashes}


class Hasher:
    """Object to hash files using multiple hash algorithms"""
    
    HASHED = 1
    NOT_HASHED = 0
    FILE_NOT_FOUND = -1
    
    default = 'md5'
    
    def __init__(self, *algorithms):
        tmp = set()
        for a in algorithms:
            tmp.add(_normalize(a))
        self._algorithms = tuple(tmp)
        if not self._algorithms:
            self._algorithms = (self.default,)
        self.hashed = 0
        self.errors = 0
        self.files = 0
        self.read_bytes = 0
        self.current_file = None
        self.current_file_size = None
        self.current_read_bytes = 0
        self.hash_records = []
        self.hash_records_errors = []
        
    @property
    def algorithms(self):
        return sorted(self._algorithms, key=lambda x: x.lower())
    
    def _hash(self, path):
        """Returns a HashRecord for file at fpath"""
        self.current_file = path
        self.current_file_size = path.stat().st_size
        self.current_read_bytes = 0
        with path.open('rb') as fin:
            if self.current_file_size >= MAX_READ_SZ:
                h = self._hash_large(fin)
            else:
                h = _Hashes(self.algorithms, fin.read())
                self.current_read_bytes += self.current_file_size
        self.read_bytes += self.current_read_bytes
        return h.hexdigests
    
    def _hash_large(self, fin):
        """Returns HashRecord of files with sizes larger than max read size"""
        data = fin.read(READ_SZ)
        h = _Hashes(self.algorithms)
        while data:
            h.update(data)
            self.current_read_bytes += len(data)
            data = fin.read(READ_SZ)
        return h
        
    def hash_file(self, path):
        """Hashes a file and returns a HashRecord"""
        try:
            h = self._hash(path)
            self.hashed += 1
            self.files += 1
            hr = HashRecord(path, self.HASHED, h)
        except FileNotFoundError as e:
            hr = HashRecord(path, self.FILE_NOT_FOUND, None, e.strerror)
            self.hash_records_errors.append(hr)
        except (IOError, OSError) as e:
            self.errors += 1
            self.files += 1
            hr = HashRecord(path, self.NOT_HASHED, None, e.strerror)
            self.hash_records_errors.append(hr)
        self.hash_records.append(hr)
        return hr
    
    def match(self, path, patterns):
        for pat in patterns:
            if path.match(pat):
                return True
        return False
    
    def hash_directory(self, directory, recursive=False, patterns=None):
        start_directory = directory.resolve()
        for root_directory, directories, filenames in os.walk(start_directory.as_posix()):
            directory = pathlib.Path(root_directory)
            for name in filenames:
                path = directory / name
                if (not patterns) or (patterns and self.match(path, patterns)):
                    yield self.hash_file(path)
            if not recursive:
                break
        self.current_file = None

class VerifyRecord(HashRecord):
    
    def __init__(self, hr, original_hashes):
        super().__init__(hr.path, hr.result, hr.hashes, hr.error_msg)
        self.original_hashes = original_hashes
    
class Verifier(Hasher):
    """Verifies hashes from previously produced hashes"""
    
    def __init__(self, *algorithms):
        super().__init__(*algorithms)
        self.matching = 0
        self.non_matching = 0
        self.not_found = 0

    def verify_hash(self, hr):
        """Returns results of verification"""
        new_hr = None
        try:
            new_hr = self.hash_file(hr.path)
        except FileNotFoundError:
            # count separately from other OSError exceptions
            self.not_found += 1
            res = self.HASH_FILE_NOT_FOUND
        # all other errors when reading
        except (IOError, OSError):
            self.total_files += 1
            self.errors += 1
            res = self.HASH_READ_ERROR
        else:
            self.total_files += 1
            self.hashed += 1
            # in case old hash is uppercase
            for alg in new_hr.hashes:
                if hr.hashes[alg] == new_hr[alg]:
                    pass
                else:
                    res = self.HASH_NO_MATCH
                    self.non_matching += 1
                    return VerifyRecord(hr, new_hr.hashes, res)
            res = self.HASH_MATCH
            self.matching += 1                
        return VerifyRecord(hr, new_hr.hashes, res)
                        
                    