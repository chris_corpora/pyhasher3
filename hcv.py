"""Command-line program to hash, copy, and verify files in the current working directory using pyhasher

"""
import shutil
import pyhasher
import argparse
import os
import logging
import tempfile
from datetime import datetime
import pyhasher

logger = logging.getLogger()
logfile_dir = tempfile.mkdtemp()
logfile_name = '{:%Y%m%d_%H%M%S}_HCV_Log.log'.format(datetime.now())
logfile_path = os.path.join(logfile_dir, logfile_name)

class ExistingDestinationError(Exception):
    pass

def hash_files(dirpath, hashfile, algorithm='md5', md5summer=False):
    """Hash files in dirpath and writes to hashfile for verification, uses pyhasher to hash files and log"""
    os.chdir(dirpath)
    args = ['-r', '-L', '-a', algorithm, '-o']
    if md5summer:
        args.append('--md5summer')
    if hashfile:
        args.append(hashfile)
    tmp = ' '.join(args)
    logging.info("Hashing files in '{src}', 'pyhasher arguments: {cargs}'".format(src=dirpath, cargs=tmp))
    cli_args = pyhasher.make_args(args)
    pyhasher.main(cli_args)

def copy_files(spath, dpath):
    """Copies all files in spath to dpath using shutil.copytree with defaults"""
    logging.info("Copying files from '{src}' to '{dst}'".format(src=spath, dst=dpath))
    try:
        shutil.copytree(spath, dpath)
    except shutil.Error as e:
        logging.error("Errors on Copying: {}".format(e))

def verify_files(dirpath, hashfile):
    """Verifies files in dirpath using hash values in hashfile, uses pyhasher to verify"""
    os.chdir(dirpath)
    args = ['-o', '-L', '-V']
    if hashfile:
        args.append(hashfile)
    tmp = ' '.join(args)
    logging.info("Verifying files in '{src}', 'pyhasher arguments: {cargs}'".format(src=dirpath, cargs=tmp))
    cli_args = pyhasher.make_args(args)
    pyhasher.main(cli_args)
    
def title(text):
    top = "-"*50
    spaced = " " + text + " "
    s = [" ", top, format(spaced, "^50"), top]
    print(os.linesep.join(s))

def run(src, dst, hashfile, algorithm):
    """Runs program"""
    spath = os.path.abspath(src)
    dpath = os.path.abspath(dst)
    if os.path.exists(dpath):
        raise ExistingDestinationError
    if hashfile:
        hashfile = os.path.abspath(hashfile)
    title("HCV: Hashing Files")
    hash_files(spath, hashfile, algorithm)
    title("HCV: Copying Files")
    print("{} --> {}".format(spath, dpath))
    copy_files(spath, dpath)
    title("HCV: Verifying Files")
    verify_files(dpath, hashfile)
    logging.info("Completed")
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('destination',
                        help='path to destination directory--it will be created and must not already exist')
    parser.add_argument('-s', '--source',
                        default='.',
                        help='path to source directory, default is current working directory')
    parser.add_argument('--hashes',
                        help='path for file containing hash values, default is to create in source root directory')
    parser.add_argument('--algorithm',
                        choices=pyhasher.Hasher.ALG_NAMES,
                        default='md5',
                        help='algorithm to use for hashing, defaults is MD5')
    parser.add_argument('--md5summer', action='store_true')
    args = parser.parse_args()
    LOG_FORMAT = '%(asctime)s %(message)s'
    logging.basicConfig(filename=logfile_path, filemode='a', level=logging.INFO, format=LOG_FORMAT)
    try:
        run(args.source, args.destination, args.hashes, args.algorithm, args.md5summer)
    except ExistingDestinationError:
        logging.critical("Destination directory already exists, program did not run")
    except (OSError, PermissionError, FileNotFoundError) as e:
        logging.critical("Did not complete: {}".format(e))
    finally:
        print()
        print(60*"=")
        print()
        msg = "Path to HCV Logfile: {}".format(logfile_path)
        print("{:^60}".format(msg))
        print()
        with open(logfile_path, 'r') as fout:
            for line in fout:
                print(line)