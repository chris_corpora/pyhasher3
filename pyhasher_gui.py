from tkinter import *
from tkinter import ttk
from tkinter import filedialog
from tkinter import scrolledtext
from pyhasher3 import *
import os
import sys
from datetime import datetime
import time
from pathlib import Path
import threading

class PyhasherGUI:
    
    def __init__(self, root):
        self.root = root
        # widthxheight+fromleft+fromtop
        self.root.geometry('1200x800+500+500')
        self.all_tabs = ttk.Notebook(self.root)
        self.all_tabs.pack(fill=BOTH, expand=True, side=TOP)
        self.text_controls = Frame(self.root)
        self.text_controls.pack(fill=X, expand=False, side=BOTTOM)
        self.hash_frame = Frame(self.all_tabs)
        self.all_tabs.add(self.hash_frame, text="Hash")
        self.verify_frame = Frame(self.all_tabs)
        self.all_tabs.add(self.verify_frame, text="Verify")        
        HasherGUI(self.hash_frame)
        VerifierGUI(self.verify_frame)

class HasherGUI:
    
    RUNNING = "RUNNING"
    STOPPED = "STOPPED"
    COMPLETED = "COMPLETED"
    
    def __init__(self, root):
        self.root = root
        self.current_dirpath = Path.cwd().resolve()
        self.top_control_frame = ttk.Frame(self.root)
        self.top_control_frame.pack(fill=X)
        self.bottom_control_frame = ttk.Frame(self.root)
        self.bottom_control_frame.pack(fill=X)
        self.separator = ttk.Separator(self.root)
        self.separator.pack(fill=X)
        self.display_frame = ttk.Frame(self.root, padding="5 20 5 20")
        self.display_frame.pack(fill=X)
        self.top_controls()
        self.display()
        self.text_control_frame = ttk.Frame(self.root)
        self.text_control_frame.pack(side=LEFT, fill=BOTH, expand=1)
        self.mk_hash_textbox()
        self.bottom_controls()
        self.counter_val = 0
        self.secs_val = 0
        self.errors_val = 0
        self.running = False
        
    def top_controls(self):
        self.top_control_frame_left = ttk.Frame(self.top_control_frame)
        self.top_control_frame_left.pack(fill=X, anchor=W, side=LEFT)
        self.top_control_frame_right = ttk.Frame(self.top_control_frame)
        self.top_control_frame_right.pack(fill=X, anchor=E, side=RIGHT)
        self.hash_combobox = ttk.Combobox(self.top_control_frame_left, state='readonly')
        self.hash_combobox['values'] = [i.upper() for i in Hasher.ALG_NAMES]
        # will make index 0 the default choice
        self.hash_combobox.current(0)
        self.hash_combobox.pack(side=LEFT)
        self.file_hash_button = ttk.Button(self.top_control_frame_left, command=self.threaded_hash_file, text="Hash File")
        self.file_hash_button.pack(side=LEFT)
        self.dir_hash_button = ttk.Button(self.top_control_frame_left, command=self.threaded_hash_dir, text="Hash Directory")
        self.dir_hash_button.pack(side=LEFT)
        self.recurs_choice = BooleanVar()
        self.recurs_checkbutton = ttk.Checkbutton(self.top_control_frame_left, variable=self.recurs_choice, text="Recurse Directory")
        self.recurs_checkbutton.pack(side=LEFT)
        
    def bottom_controls(self):
        self.bottom_controlframe = ttk.Frame(self.root)
        self.bottom_controlframe.pack(side=BOTTOM)      
        
    def display(self):
        self.secs = StringVar()
        self.secs.set("0 Seconds")
        self.secs_label = ttk.Label(self.display_frame, textvariable=self.secs)
        self.secs_label.pack(side=BOTTOM, anchor=W)
        self.counter = StringVar()
        self.counter_label = ttk.Label(self.display_frame, textvariable=self.counter)
        self.counter_label.pack(side=BOTTOM, anchor=W)
        self.errors = StringVar()
        self.errors_label = ttk.Label(self.display_frame, textvariable=self.errors)
        self.errors_label.pack(side=BOTTOM, anchor=W)
        self.errors.set("0 Errors")
        self.counter.set("0 Files Hashed")
        self.run_state = StringVar()
        self.run_state.set(self.STOPPED)
        self.run_state_label = ttk.Label(self.display_frame, textvariable=self.run_state)
        self.run_state_label.pack(side=LEFT)
        
    def mk_hash_textbox(self):
        self.text_control_frame_top = ttk.Frame(self.text_control_frame)
        self.text_control_frame_top.pack(side=TOP, fill=BOTH, expand=1)
        self.text_control_frame_bottom = ttk.Frame(self.text_control_frame)
        self.text_control_frame_bottom.pack(side=BOTTOM, fill=X, anchor=N)
        self.hash_textbox = Text(self.text_control_frame_top)
        self.hash_textbox.config(wrap=NONE) 
        self.hash_textbox_yscroll = ttk.Scrollbar(self.text_control_frame_top)
        self.hash_textbox_yscroll.config(command=self.hash_textbox.yview)
        self.hash_textbox_yscroll.pack(anchor=E, side=RIGHT, fill=Y, expand=FALSE)
        self.hash_textbox_xscroll = ttk.Scrollbar(self.text_control_frame_bottom)
        self.hash_textbox_xscroll.config(command=self.hash_textbox.xview)
        self.hash_textbox_xscroll.config(orient=HORIZONTAL)
        self.hash_textbox_xscroll.pack(anchor=N, fill=X, expand=FALSE)
        self.hash_textbox.pack(anchor=NW, fill=BOTH, side=TOP, expand=TRUE)       
        # required for scrollbar slider to change
        self.hash_textbox.config(yscrollcommand=self.hash_textbox_yscroll.set)
        self.hash_textbox.config(xscrollcommand=self.hash_textbox_xscroll.set)
        self.save_to_file_button = ttk.Button(self.text_control_frame_bottom, command=self.save_to_file, text="Save to File")
        self.save_to_file_button.pack(side=LEFT)
        self.copy_to_clipboard_button = ttk.Button(self.text_control_frame_bottom, command=self.copy_to_clipboard, text="Copy to Clipboard")
        self.copy_to_clipboard_button.pack(side=LEFT)        
        self.clear_hash_textbox_button = ttk.Button(self.text_control_frame_bottom, command=self.clear_hash_textbox, text="Clear Text")
        self.clear_hash_textbox_button.pack(side=LEFT)        

    def update_timer(self):
        while self.running:
            self.secs_val += 1
            self.secs.set("{:,} Seconds".format(self.secs_val))
            time.sleep(1)
    
    def get_file_hash(self):
        hasher = Hasher(Hasher.ALG_NAMES[self.hash_combobox.current()])
        fn = filedialog.askopenfilename(initialdir=str(self.current_dirpath))
        if fn == '':
            return
        self.current_dirpath = Path(fn).resolve().parent
        self.run_state.set(self.RUNNING)
        self.running = True
        timer = threading.Thread(target=self.update_timer)
        timer.start()
        # used to show updated run state before starting to hash, otherwise stills show last run state
        self.root.update()
        fpath = Path(fn).resolve()
        h = hasher.hash_file(fpath)
        self.counter_val += hasher.hashed
        self.errors_val += hasher.errors
        self.counter.set("{:,} Files Hashed".format(self.counter_val))
        self.errors.set("{:,} Errors".format(self.errors_val))
        s = HashRunner.SEP.join([h, fpath.name]) + "\n"
        self.hash_textbox.insert(END, s)
        self.hash_textbox.update()
        self.run_state.set(self.COMPLETED)
        self.running = False
        
    def get_dir_hash(self):
        self.clear_hash_textbox()
        hasher = Hasher(Hasher.ALG_NAMES[self.hash_combobox.current()])
        dirpath = filedialog.askdirectory(initialdir=str(self.current_dirpath.parent))
        if dirpath == '':
            return
        self.current_dirpath = Path(dirpath).resolve()
        all_files = hasher.hash_files(self.current_dirpath, recurs=self.recurs_choice.get())
        self.run_state.set(self.RUNNING)
        self.running = True
        timer = threading.Thread(target=self.update_timer)
        timer.start()
        for h, fpath in all_files:
            self.counter.set("{:,} Files Hashed".format(hasher.hashed))
            self.errors.set("{:,} Errors".format(hasher.errors))
            s = HashRunner.SEP.join([h, str(fpath.relative_to(dirpath))]) + "\n"
            self.hash_textbox.insert(END, s)
            self.hash_textbox.update()
        self.run_state.set(self.COMPLETED)
        self.running = False
            
    def copy_to_clipboard(self):
        all_text = self.hash_textbox.get('1.0', END)
        self.root.clipboard_clear()
        self.root.clipboard_append(all_text)
        
    def save_to_file(self):
        ext = '.{}'.format(Hasher.ALG_NAMES[self.hash_combobox.current()].lower())
        logfile_fn = 'hashes' + ext
        all_text = self.hash_textbox.get('1.0', END)
        dirpath = str(self.current_dirpath)
        save_filepath = filedialog.asksaveasfilename(initialdir=dirpath, initialfile=logfile_fn)
        with open(save_filepath, 'w', encoding='utf-8') as save_file:
            save_file.write(all_text)
    
    def clear_hash_textbox(self):
        # clear from line 1 char 0 to the end
        self.hash_textbox.delete('1.0', END)
        self.run_state.set(self.STOPPED)
        self.secs.set("{:,} Seconds".format(0))
        self.counter.set("{:,} Files Hashed".format(0))
        self.errors.set("{:,} Errors".format(0))
        
    def threaded_hash_dir(self):
        p = threading.Thread(target=self.get_dir_hash)
        p.start()
        
    def threaded_hash_file(self):
        p = threading.Thread(target=self.get_file_hash)
        p.start()
        
class VerifierGUI(HasherGUI):
    
    def __init__(self, root):
        self.verify_path = StringVar()
        self.verify_root_directory = StringVar()
        super().__init__(root)
    
    def top_controls(self):
        file_frame = ttk.Frame(self.top_control_frame)
        file_frame.pack(expand=True, fill=X)
        root_frame = ttk.Frame(self.top_control_frame)
        root_frame.pack(expand=True, fill=X)
        start_frame = ttk.Frame(self.top_control_frame)
        start_frame.pack(expand=True, fill=X)
        ttk.Label(file_frame, text="Verify File:        ").pack(side=LEFT)
        ttk.Label(root_frame, text="Root Directory: ").pack(side=LEFT)
        file_entry = Entry(file_frame, textvariable=self.verify_path)
        file_entry.pack(expand=True, side=LEFT, fill=X)
        file_entry_browse = ttk.Button(file_frame, command=self.get_verify_filepath)
        file_entry_browse.configure(text="Browse")
        file_entry_browse.pack(side=LEFT, expand=False)        
        root_directory_entry = Entry(root_frame, textvariable=self.verify_root_directory )
        root_directory_entry.pack(expand=True, side=LEFT, fill=X)
        root_directory_entry_browse = ttk.Button(root_frame, command=self.get_verify_root_directory)
        root_directory_entry_browse.configure(text="Browse")
        root_directory_entry_browse.pack(side=LEFT, expand=False)
        verify_start = ttk.Button(start_frame, command=self.verify_hashes)
        verify_start.pack()
        verify_start.configure(text="Start Verification")

        
    def verify_hashes(self):
        args = "--verify {} --dirpath {}".format(self.verify_path, self.verify_root_directory)
        cli_args = make_args(args.split())
        vrunner = get_runner(cli_args)
        vrunner()
    
    def get_verify_filepath(self):
        fn = filedialog.askopenfilename()
        self.verify_path.set(fn)
        
    def get_verify_root_directory(self):
        fn = filedialog.askdirectory()
        self.verify_root_directory.set(fn)
        
    
if __name__ == "__main__":
    root = Tk()
    root.title("pyhasher, version {}".format(VERSION))
    app = PyhasherGUI(root)
    app.root.mainloop()