pyhasher
========

Version 3b

*pyhasher* is a cross-platform, command-line Python program for hashing files in a directory or directory tree.

This is an updated version that is in flux